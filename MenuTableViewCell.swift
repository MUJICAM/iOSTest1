//
//  MenuTableViewCell.swift
//  MenuBarraLateral
//
//  Created by Hostienda Hostienda on 18/8/17.
//  Copyright © 2017 MujicaM9. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var NombreCeldaMenu: UILabel!
    @IBOutlet weak var iconoMenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
