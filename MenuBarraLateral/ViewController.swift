//
//  ViewController.swift
//  MenuBarraLateral
//
//  Created by Hostienda Hostienda on 18/8/17.
//  Copyright © 2017 MujicaM9. All rights reserved.
//

import UIKit
import  FBSDKLoginKit
import FacebookLogin

class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    //dimenciones de la ventana
   
    let defaults:UserDefaults = UserDefaults.standard
    
    
    var loginFacebook = false
    let ancho = UIScreen.main.bounds.width
    let largo = UIScreen.main.bounds.height
    var login: FBSDKLoginManager = FBSDKLoginManager()
    
    var salida = false
    
    @IBOutlet weak var botonIngresar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(salida){
          self.login.logOut()
          self.loginFacebook = false
            FBSDKAccessToken.setCurrent(nil)
        }
        
        let botonLoginFacebook = FBSDKLoginButton()
        botonLoginFacebook.frame = CGRect(x: ancho * 0.10, y: largo * 0.70, width: view.frame.width - 80, height: view.frame.height * 0.05)
        
        botonLoginFacebook.readPermissions = ["email"]
        
        view.addSubview(botonLoginFacebook)
        botonLoginFacebook.delegate = self
        if (FBSDKAccessToken.current()) != nil{
            fetchProfile()
        }
        
        
        //SWRevealViewController revealController = self.revealViewController()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func operacion (){
        
        let menu = (self.storyboard?.instantiateViewController(withIdentifier: "Menu"))! as! SWRevealViewController
        self.navigationController?.pushViewController(menu, animated: true)

    }
    
    //****************Logeo y Deslogeo
    
    func fetchProfile(){
        let parametros = ["fields": "email, first_name, last_name, picture.type(large)"]
        print("fetchProfile")
        FBSDKGraphRequest(graphPath: "me", parameters: parametros).start{ ( connection, data, error) -> Void in
            
            if error != nil {
                print("ERROR:\n" , error!)
                return
            }
            
            if let result:[String : AnyObject] = data as? [String : AnyObject]{
                self.loginFacebook=true
                
                let nombre = result["first_name"] as! String
                let apellido = result["last_name"] as! String
                let correo = result["email"] as! String
                let fotoPerfil = (result["picture"]!["data"]!! as! [String : AnyObject])["url"]
                
                self.defaults.set(nombre + " " + apellido ,forKey: "fullnombre")
                self.defaults.set(correo, forKey: "correo")
                self.defaults.set(fotoPerfil, forKey: "foto")
                
                self.operacion()
                //self.performSegue(withIdentifier: "Gogo", sender: nil)
                
            }
            
            
            
        }
    }
    
    
    
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Se Deslogeo")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if(error != nil){
            print(error)
            return
        }else if result.isCancelled{
            print("CANCELADO")
        }else{
            print("Logeado Exitosamente...")
            fetchProfile()
        }
        
    }
    
    //***********fin de deslogeo y logeo
    
    @IBAction func accionTnT(_ sender: Any) {
       print("ENTRO")
        
        let menu = (self.storyboard?.instantiateViewController(withIdentifier: "Menu"))! as! SWRevealViewController
        
        self.navigationController?.pushViewController(menu, animated: true)
        
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //self.tabBarController?.navigationItem.hidesBackButton=true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

