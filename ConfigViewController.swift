//
//  ConfigViewController.swift
//  MenuBarraLateral
//
//  Created by Hostienda Hostienda on 18/8/17.
//  Copyright © 2017 MujicaM9. All rights reserved.
//

import UIKit

class ConfigViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var configNames:Array = [String]()
    // let loginManager = FBSDKLoginManager()
    
    @IBOutlet weak var tablaConfig: UITableView!
    @IBOutlet weak var botonMenu: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNames = ["Cerrar Sesion"]
        
        self.tablaConfig.dataSource = self
        self.tablaConfig.delegate = self
        botonMenu.target = revealViewController()
        botonMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*** Accion y funciones de la tabla***/
    //1
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configNames.count
    }
    
    //2
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfiguracionTableViewCell") as! ConfiguracionTableViewCell
        cell.opcionName.text! = configNames[indexPath.row]
        return cell
    }
    
    //3
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let revealViewController: SWRevealViewController = self.revealViewController()
        let cell:ConfiguracionTableViewCell = tableView.cellForRow(at: indexPath) as! ConfiguracionTableViewCell
        
        if(cell.opcionName.text == "Cerrar Sesion"){
            print("Cerrar")
            self.CerrarSesion()
        }
    }
     
 

    
    /*** FIN ***/
    
    func CerrarSesion(){
       let navigationController = (self.storyboard?.instantiateViewController(withIdentifier: "ViewController"))! as UIViewController
        self.present(navigationController, animated: true, completion: nil)
    }
}
