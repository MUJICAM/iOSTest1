//
//  MenuViewController.swift
//  MenuBarraLateral
//
//  Created by Hostienda Hostienda on 18/8/17.
//  Copyright © 2017 MujicaM9. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var menuNameArr:Array = [String]()
    var iconArr: Array=[UIImage]()
    
    @IBOutlet weak var nombreUsuario: UILabel!
    @IBOutlet weak var fotoUsuario: UIImageView!
    
    let defaults:UserDefaults = UserDefaults.standard
    /*  */

    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuNameArr = ["Inicio","Mensajes","Mapas","Configuraciones"]
        iconArr = [UIImage(named: "home")!,UIImage(named: "message")!,UIImage(named: "map")!,UIImage(named: "setting")!]
        
        self.nombreUsuario.text = self.defaults.string(forKey: "fullnombre")
        
        self.DescargaImagenPerfil(self.defaults.string(forKey: "foto")!, inView: fotoUsuario)
        // Do any additional setup after loading the view.
    }
    
    func DescargaImagenPerfil(_ uri : String, inView: UIImageView){
        
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil{
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print(error!)
            }
        }
        
        task.resume()
        
    }
    
    /*  */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        cell.NombreCeldaMenu.text! = menuNameArr[indexPath.row]
        cell.iconoMenu.image = iconArr[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealViewController: SWRevealViewController = self.revealViewController()
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        
        if(cell.NombreCeldaMenu.text! == "Inicio"){
            let mainStoryBoiard: UIStoryboard = UIStoryboard(name:"Main",bundle:nil)
            let desViewController = mainStoryBoiard.instantiateViewController(withIdentifier: "InicioViewController") as! InicioViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController:desViewController)
            
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
  
        }
        
        if (cell.NombreCeldaMenu.text == "Mensajes"){
            let mainStoryboiard: UIStoryboard = UIStoryboard(name: "Main",bundle:nil)
            let desController = mainStoryboiard.instantiateViewController(withIdentifier: "MensajeViewController") as! MensajeViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        if (cell.NombreCeldaMenu.text == "Mapas"){
            let mainStoryboiard: UIStoryboard = UIStoryboard(name: "Main",bundle:nil)
            let desController = mainStoryboiard.instantiateViewController(withIdentifier: "MapasViewController") as! MapasViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
        
        if (cell.NombreCeldaMenu.text == "Configuraciones"){
            let mainStoryboiard: UIStoryboard = UIStoryboard(name: "Main",bundle:nil)
            let desController = mainStoryboiard.instantiateViewController(withIdentifier: "ConfigViewController") as! ConfigViewController
            
            let newFrontViewController = UINavigationController.init(rootViewController: desController)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
            
        }
    }
    
    /*  */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
